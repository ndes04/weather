﻿namespace Weather
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.areaBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.都市名DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.天気DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.温度DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.weatherDataTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.weatherDataSet = new Weather.WeatherDataSet();
            this.readButton = new System.Windows.Forms.Button();
            this.removeButton = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.weatherDataTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.weatherDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // areaBox
            // 
            this.areaBox.FormattingEnabled = true;
            this.areaBox.Location = new System.Drawing.Point(14, 208);
            this.areaBox.Name = "areaBox";
            this.areaBox.Size = new System.Drawing.Size(121, 20);
            this.areaBox.TabIndex = 0;
            this.areaBox.SelectedIndexChanged += new System.EventHandler(this.CitySelected);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 182);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "都道府県選択";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.都市名DataGridViewTextBoxColumn,
            this.天気DataGridViewTextBoxColumn,
            this.温度DataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.weatherDataTableBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(12, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 21;
            this.dataGridView1.Size = new System.Drawing.Size(348, 123);
            this.dataGridView1.TabIndex = 3;
            // 
            // 都市名DataGridViewTextBoxColumn
            // 
            this.都市名DataGridViewTextBoxColumn.DataPropertyName = "都市名";
            this.都市名DataGridViewTextBoxColumn.HeaderText = "都市名";
            this.都市名DataGridViewTextBoxColumn.Name = "都市名DataGridViewTextBoxColumn";
            // 
            // 天気DataGridViewTextBoxColumn
            // 
            this.天気DataGridViewTextBoxColumn.DataPropertyName = "天気";
            this.天気DataGridViewTextBoxColumn.HeaderText = "天気";
            this.天気DataGridViewTextBoxColumn.Name = "天気DataGridViewTextBoxColumn";
            // 
            // 温度DataGridViewTextBoxColumn
            // 
            this.温度DataGridViewTextBoxColumn.DataPropertyName = "温度";
            this.温度DataGridViewTextBoxColumn.HeaderText = "温度";
            this.温度DataGridViewTextBoxColumn.Name = "温度DataGridViewTextBoxColumn";
            // 
            // weatherDataTableBindingSource
            // 
            this.weatherDataTableBindingSource.DataMember = "weatherDataTable";
            this.weatherDataTableBindingSource.DataSource = this.weatherDataSet;
            // 
            // weatherDataSet
            // 
            this.weatherDataSet.DataSetName = "WeatherDataSet";
            this.weatherDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // readButton
            // 
            this.readButton.Location = new System.Drawing.Point(265, 161);
            this.readButton.Name = "readButton";
            this.readButton.Size = new System.Drawing.Size(75, 23);
            this.readButton.TabIndex = 4;
            this.readButton.Text = "読み込む";
            this.readButton.UseVisualStyleBackColor = true;
            this.readButton.Click += new System.EventHandler(this.ReadButtonClicked);
            // 
            // removeButton
            // 
            this.removeButton.Location = new System.Drawing.Point(265, 191);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(75, 23);
            this.removeButton.TabIndex = 5;
            this.removeButton.Text = "削除";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.RemoveButtonClicked);
            // 
            // closeButton
            // 
            this.closeButton.Location = new System.Drawing.Point(265, 220);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75, 23);
            this.closeButton.TabIndex = 6;
            this.closeButton.Text = "閉じる";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.CloseButtonClicked);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(372, 266);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.readButton);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.areaBox);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.weatherDataTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.weatherDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox areaBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn 都市名DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 天気DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 温度DataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource weatherDataTableBindingSource;
        private WeatherDataSet weatherDataSet;
        private System.Windows.Forms.Button readButton;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.Button closeButton;
    }
}

