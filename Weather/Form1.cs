﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using Newtonsoft.Json.Linq;

namespace Weather
{
    public partial class Form1 : Form
    {
        Dictionary<string, string> cityNames;
        string city;
        string weather;
        string temp;
        public Form1()
        {
            InitializeComponent();

            this.cityNames = new Dictionary<string, string>();

            this.cityNames.Add("東京都", "130010");
            this.cityNames.Add("大阪府", "1");
            this.cityNames.Add("愛知県", "2");
            this.cityNames.Add("福岡県", "10");

            foreach (KeyValuePair<string, string> data in this.cityNames)
            {
                areaBox.Items.Add(data.Key);
            }
        }

        private void CitySelected(object sender, EventArgs e)
        {
            //天気情報サービスにアクセスする
            string cityCode = cityNames[areaBox.Text];
            string url = "http://weather.livedoor.com/forecast/webservice/json/v1" + cityCode;

            HttpClient client = new HttpClient();
            string result = client.GetStringAsync(url).Result;
            

            //天気情報からアイコンのURLを取り出す
            JObject jobj = JObject.Parse(result);
            city = (string)((jobj["city"] as JValue).Value);
            dataGridView1.Text =city;

            weather = (string)((jobj["weather"] as JValue).Value);
            dataGridView1.Text = weather;



            temp = (string)((jobj["temp"] as JValue).Value);
            dataGridView1.Text= temp;

        }

        private void ReadButtonClicked(object sender, EventArgs e)
        {
            //DataTableにデータを追加する
            weatherDataSet.weatherDataTable.AddweatherDataTableRow(
                this.city, this.weather,int.Parse(this.temp));
        }

        private void RemoveButtonClicked(object sender, EventArgs e)
        {
            //選択した行のデータを削除する
            int row = this.dataGridView1.CurrentRow.Index;
            this.dataGridView1.Rows.RemoveAt(row);
        }

        private void CloseButtonClicked(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
